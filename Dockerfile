FROM python:3.10.4-slim-bullseye
ENV PIP_DISABLE_PIP_VERSION_CHECK 1
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
WORKDIR /code
RUN apt-get update && \
    apt-get install -y build-essential libpq-dev && \
    apt-get clean
COPY ./requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt
COPY . .


